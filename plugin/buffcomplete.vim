" Simplest autocompletion
" http://stackoverflow.com/a/41432390/1068046
" http://vi.stackexchange.com/a/10096/5232
" http://vi.stackexchange.com/a/2467/5232
if v:version >= 704 && has('patch-7-4-755')
    " settings
    set completeopt+=noinsert
    inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
    " main
    augroup BuffCompleteAu
        autocmd!
        autocmd InsertCharPre * call <SID>PopUpDict()
    augroup END
    let s:count=0
    function! s:PopUpDict()
        let AsciiCode=char2nr(v:char)
        if (AsciiCode <=# 122 && AsciiCode >=# 97) || (AsciiCode <=# 90 && AsciiCode >=# 65) || (AsciiCode <=# 1103 && AsciiCode >=# 1040)
            let s:count+=1
            if s:count >=# 3
            call feedkeys("\<c-x>\<c-p>")
            endif
        else
            let s:count=0
        endif
    endfunction
else
    echohl WarningMsg
    echo "WARNING: Your Vim is too old (version 7.4.755 needed)."
    echohl None
endif
