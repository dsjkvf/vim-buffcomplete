# Completing words in a current buffer

## About

This is a Vim plugin for completing words in a current buffer (can only complete those words, which are already present in a buffer). Originally written by [Meninx](http://stackoverflow.com/a/41432390/1068046), made now to a separate plugin in order to make the installation process easier.

Requires Vim 7.4 and [patch 755](https://groups.google.com/forum/#!topic/vim_dev/9BGZNGA7Ucg).
